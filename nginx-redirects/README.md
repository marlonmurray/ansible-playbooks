## This playbook will SSH to a remote server and setup a new domain in Nginx with letsencrypt SSL certificate.  

Steps 
1. Update the ansible_ssh_private_key_file to match your SSH keyfile location
2. Change the domain to the servers IP address in DNS.  (Both non-www and www)
3. Run the playbook 
```bash
ansible-playbook -i inventory.yml nginx.yml --extra-vars "domain=<domain> destination=<destination domain>" 
```